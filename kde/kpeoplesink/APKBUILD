# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kpeoplesink
pkgver=0_git20201129
pkgrel=0
_commit="9c14f17cefec6e56467b19130febe2eff734d3b7"
pkgdesc="Expose Sink contacts to KPeople"
url="https://invent.kde.org/pim/kpeoplesink"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
license="LGPL-2.1-or-later"
makedepends="
	extra-cmake-modules
	kaccounts-integration-dev
	kcontacts-dev
	kcoreaddons-dev
	kpeople-dev
	qt5-qtbase-dev
	sink-dev
	"
source="https://invent.kde.org/pim/kpeoplesink/-/archive/$_commit/kpeoplesink-$_commit.tar.gz"
options="!check" # Broken tests
builddir="$srcdir/$pkgname-$_commit"

prepare() {
	default_prepare

	# qmlplugindump fails for armv7+qemu (pmb#1970). This is purely for
	# packager knowledge and doesn't affect runtime, so we can disable it.
	if [ "$CARCH" = "armv7" ]; then
		sed -i "s/ecm_find_qmlmodule/# ecm_find_qmlmodule/g" CMakeLists.txt
	fi
}

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="3da17501e617637ab0e54e6062afa82dfd87fe3f2e5d9c0b0305d5477aa56c0aedce150f29c01e767f8819ed9d854304d20281dec18513e38ab45bf0d7dd8266  kpeoplesink-9c14f17cefec6e56467b19130febe2eff734d3b7.tar.gz"
